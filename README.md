[![pipeline status](https://gitlab.com/python-shit/tgmpd/badges/master/pipeline.svg)](https://gitlab.com/python-shit/tgmpd/commits/master) [![coverage report](https://gitlab.com/python-shit/tgmpd/badges/master/coverage.svg)](https://gitlab.com/python-shit/tgmpd/commits/master)
# Telegram audio to MPD bot

## Install ffmpeg

```bash
sudo apt-get install --no-install-recommends -y ffmpeg
```

## Setup virtualenv

```bash
virtualenv -p python3 venv
. venv/bin/activate
pip install -r requirements.txt
```

## Create config.env

```bash
cat <<EOF > config.env
API_TOKEN=token
MPD_HOST=pass@localhost
MPD_PORT=6600
MPD_MUSIC_DIRECTORY=/tmp/mpd
MUSIC_DIRECTORY=Telegram
EOF
```

## Run application

```bash
env $(grep -v "^#" config.env) python -m tgmpd
```
