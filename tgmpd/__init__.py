import asyncio
import logging
import os

from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.types import ContentTypes
from aiogram.utils.exceptions import BadRequest
from aiogram.utils.executor import start_polling

from .utils import mpd_current_song, mpd_add_directory
from .utils import youtube_dl_info, youtube_dl_download


async def send_welcome(message: types.Message):
    await message.reply('\n'.join((
        'Hi!', 'I\'m MPDBot!', 'Powered by misuzu.',
        'Send me some audio and i will add it to queue!')))


async def current_song(message: types.Message):
    song = await mpd_current_song()
    if song.startswith(os.environ['MUSIC_DIRECTORY']):
        song = os.path.basename(song)
    await message.reply(song)


async def add_song(message: types.Message):
    if message.audio:
        audio = message.audio
        name = '%s - %s' % (audio.performer.strip(), audio.title.strip())
        file_name = '%s.mp3' % name
    elif message.document and message.document.mime_type.startswith('audio'):
        audio = message.document
        file_name = audio.file_name
    mpd_path = os.path.join(
        os.environ['MUSIC_DIRECTORY'], 'Telegram', audio.file_id)
    file_path = os.path.join(
        os.environ['MPD_MUSIC_DIRECTORY'], mpd_path, file_name)
    os.makedirs(os.path.dirname(file_path), exist_ok=True)

    if not os.path.isfile(file_path):
        await message.reply('Downloading...')
        logging.info('Downloading %r to %r', file_name, mpd_path)
        try:
            await audio.download(file_path)
        except BadRequest as e:
            logging.error('Download error: %s', e)
            return await message.reply('File is too big')
        else:
            logging.info('Downloaded %r to %r', file_name, mpd_path)
    else:
        logging.info('File %r in %r already exists', name, mpd_path)

    if (await mpd_add_directory(mpd_path)):
        logging.info('Added %r to queue', file_name)
        return await message.reply('Added to queue')
    logging.warning('Error when adding %r to queue', name)
    await message.reply('Error when adding to queue')


async def handle_link(message: types.Message):
    for entity in message.entities:
        if entity.type == 'url':
            url = message.text[entity.offset:entity.offset + entity.length]
            info = await youtube_dl_info(url)
            name = (info.get('uploader_id') or
                    info.get('playlist_uploader_id') or
                    info.get('uploader'))
            if not name:
                logging.info('Unknown link: %r, %r', url, info)
                return await message.reply('Unknown link')
            root = info.get('extractor_key') or 'Youtube-DL'
            playlist = info.get('playlist') or info.get('title') or 'NA'
            mpd_path = os.path.join(
                os.environ['MUSIC_DIRECTORY'], root, name, playlist)
            path = os.path.join(
                os.environ['MPD_MUSIC_DIRECTORY'], mpd_path)
            os.makedirs(path, exist_ok=True)
            await message.reply('Downloading...')
            if ((await youtube_dl_download(path, url)) and
                    (await mpd_add_directory(mpd_path))):
                logging.info('Added %r to queue', playlist)
                return await message.reply('Added to queue')
            else:
                return await message.reply('Error occured')
    await message.reply('No link found')


def start():
    logging.basicConfig(
        format='[%(levelname)1.1s %(asctime)s.%(msecs)03d '
               '%(process)d] %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S',
        level=logging.INFO)

    loop = asyncio.get_event_loop()

    bot = Bot(token=os.environ['API_TOKEN'], loop=loop)

    dp = Dispatcher(bot)
    dp.register_message_handler(send_welcome, commands=['start', 'help'])
    dp.register_message_handler(current_song, commands=['current'])
    dp.register_message_handler(
        add_song, content_types=ContentTypes.AUDIO & ContentTypes.DOCUMENT)
    dp.register_message_handler(handle_link, content_types=ContentTypes.ANY)

    start_polling(dp, loop=loop, skip_updates=False)
