import asyncio
import json
import os


async def mpd_current_song():
    p = await asyncio.create_subprocess_exec(
        'mpc', 'current', stdout=asyncio.subprocess.PIPE)
    song = (await p.stdout.readline()).decode().strip()
    await p.wait()
    return song


async def mpd_add_directory(mpd_path):
    root_path = mpd_path
    while root_path and root_path != '/':
        if not (await (await asyncio.create_subprocess_exec(
           'mpc', 'update', '-q', '--wait', root_path)).wait()):
            if not (await (await asyncio.create_subprocess_exec(
               'mpc', 'insert', mpd_path)).wait()):
                return True
        root_path = os.path.dirname(root_path)
    return False


async def youtube_dl_download(path, url):
    output = os.path.join(path, "%(playlist_index)s. %(title)s.%(ext)s")
    if 'youtu' in url:
        cmd = ('youtube-dl', '-f', 'm4a', '-o', output, url)
    else:
        cmd = ('youtube-dl', '--audio-format', 'mp3', '-o', output, url)
    return not (await (await asyncio.create_subprocess_exec(*cmd)).wait())


async def youtube_dl_info(url):
    p = await asyncio.create_subprocess_exec(
        'youtube-dl', '-j', '--max-downloads', '1', url,
        stdout=asyncio.subprocess.PIPE)
    result = json.loads((await p.stdout.read()).decode())
    await p.wait()
    return result
