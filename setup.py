#!/usr/bin/env python
import os
import setuptools


def read(*paths):
    with open(os.path.join(*paths), 'r') as f:
        return f.read()


setuptools.setup(
    name='tgmpd',
    version='0.0.3',
    description='Play music from Telegram on MPD',
    long_description=read('README.md'),
    author='misuzu',
    install_requires=[
        'aiogram < 3',
        'youtube-dl',
    ],
    packages=setuptools.find_packages(
        exclude=['*.tests', '*.tests.*', 'tests.*', 'tests']),
    entry_points={
        'console_scripts': ['tgmpd=tgmpd:start'],
    },
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    test_suite='tests',
)
