FROM debian:buster-slim as builder

RUN apt-get update

RUN apt-get install -y ca-certificates python3 python3-setuptools
RUN apt-get install -y build-essential python3-dev virtualenv

COPY . /app
WORKDIR /app

RUN virtualenv -p python3 /venv && . /venv/bin/activate && \
    pip install /app


FROM debian:buster-slim as final

RUN apt-get update

RUN apt-get install -y ca-certificates python3 python3-setuptools
RUN apt-get install -y ffmpeg mpc
RUN apt-get clean

COPY --from=builder /venv /venv


CMD ["/bin/bash", "-c", ". /venv/bin/activate && exec python -m tgmpd"]
